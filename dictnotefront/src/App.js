import logo from './logo.svg';
import './App.css';
import Word from './components/Word'
import Search from './components/Search'
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Know some words here</h1>
        <br/>
        <br/>
        <Search/>
        <br/>
        <br/>
      {/*<Word/>*/}
      </header>
    </div>
  );
}

export default App;
