import React, { useState, useEffect } from 'react';
export default function Word(word = "hello"){

//const response = await fetch(" https://api.dictionaryapi.dev/api/v2/entries/en/vague");
//const data = await response.json();
  const [result,setResult] = useState([]);
  const [isLoading,setIsLoading]=useState(false);
  const [isError,setIsError] = useState(false);
  const [definition,setDefinition] = useState([]);
  const [synonym,setSynonym] = useState([]);
  const [antonym,setAntonym] = useState([]);
  //const [type,setType] = useState('');


  const fetchData = () => {
    fetch(`https://api.dictionaryapi.dev/api/v2/entries/en/${word}`)
      .then((response) => response.json())
      .then((data) => {
        setIsLoading(false);
        setResult(data);
      //  console.log(result);
        setDefinition(result['0'].meanings['0'].definitions['0'].definition)
        console.log(definition);
        setSynonym(result['0'].meanings['0'].definitions['0'].synonyms.slice(0,3));
        console.log(synonym);
          setAntonym(result['0'].meanings['0'].definitions['0'].antonyms.slice(0,3))
          console.log(antonym)
        //setSynonym(result[0].meanings[0].definitions)
      })
      .catch((error) => {
        setIsLoading(false);
        setIsError(true);
        console.log(error);
      })
  }

  useEffect(() => {
    fetchData();

  },[word]);
  if(isLoading){
    return <div> loading </div>
  }

  const synonymList = synonym.map(syn => <li>{syn}</li>);
  const antonymList = antonym.map(ant => <li>{ant}</li>);
  return(
    <div>
      <h1>Result</h1>
      {synonym.length > 0 && <h2>Synonyms</h2>}
      <ul>{synonymList.length > 1 && synonymList}</ul><br/>
    { antonym.length > 0&& <h2>Antonym</h2>}
      <ul>{antonymList}</ul>
      {/*isError && <div>Error fetching data </div>*/}
    </div>
  )
}
