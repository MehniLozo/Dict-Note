//Starting with a normal manipulation of the API
import fetch from 'node-fetch';
const response = await fetch(" https://api.dictionaryapi.dev/api/v2/entries/en/vague");
const data = await response.json();

console.log("Synonym")
console.log(data[0].meanings[0].definitions[0].synonyms);
console.log("Antonym ")
console.log(data[0].meanings[0].definitions[0].antonyms);
//const express = require("express");
import express from 'express';
const app = express();
const port = 3000;

console.log(typeof express);

app.get('/',(req,res) => {
  res.send(data[0].meanings[0].definitions[0].synonyms);
})

app.listen(port,() => {
  console.log(`listening at http://localhost:${port}`);
})
